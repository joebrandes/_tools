Hier landen die Sicherungsdateien - erstellt mit den Batch-Skripten!

Oder aber Ablage von Demo-Installationen für Joomla, WordPress oder TYPO3.

Für CMSOD Version 5.0 mit PHP 8.2.12 habe ich während der Erstellung
des neuen XAMPP-CMSOD Basisinstallationen (siehe Demos) erstellt und gesichert:

Mustermann-Max_joomla_joomla-demo_20250204.zip
Mustermann-Max_typo3_www.typo3-demo.local_20250204.zip
Mustermann-Max_wordpress_wordpress-demo_20250204.zip

Diese können zu den Seminaren bereitgestellt werden.