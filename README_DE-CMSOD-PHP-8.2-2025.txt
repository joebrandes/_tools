  ____  __  __  ____    ___   ____
 / ___||  \/  |/ ___|  / _ \ |  _ \
| |    | |\/| |\___ \ | | | || | | |
| |___ | |  | | ___) || |_| || |_| |
 \____||_|  |_||____/  \___/ |____/


Logbuch CMSOD 5.0-2025 (Überarbeitungen 2025)
======================

Ordner _tools mit Tagging als Gitlab-Projekt weiter genutzt 
https://gitlab.com/joebrandes/_tools

Ordner _tools im Projekt C:\xampp-cmsod\ ohne Gitordner .git !!
Anm.: wegen Größe!

Rohversion XAMPP-CMSOD
XAMPP Version 8.2.12 mit:
	Apache 2.4.58
	MariaDB 10.4.32
	PHP 8.2.12
	phpMyAdmin 5.2.1
	OpenSSL 3.1.3
	XAMPP Control Panel 3.3.0
	Curl 8.4.0_6


Logbuch CMSOD 4.0-2023 (Überarbeitungen 2023)
======================

Ordner _tools mit Tagging als Gitlab-Projekt weiter genutzt 
https://gitlab.com/joebrandes/_tools

Ordner _tools im Projekt C:\xampp-cmsod\ ohne Gitordner .git !!
Anm.: wegen Größe!

Rohversion XAMPP-CMSOD
XAMPP Version 8.1.12 mit:
	Apache 2.4.54
	MariaDB 10.4.27
	PHP 8.1.12
	phpMyAdmin 5.2.0
	OpenSSL 1.1.1p
	XAMPP Control Panel 3.3.0



folgende Version mit Gitlab-Projekt Tag v7.4

Logbuch CMSOD 4.0: (Überarbeitungen / Einführung 2021):
==================
Rohversion XAMPP-CMSOD (Anm.: ohne Musterprüfungen)
XAMPP Version 7.4.14 mit:
	Apache 2.4.46
	MariaDB 10.4.17
	PHP 7.4.14
	phpMyAdmin 5.0.4
	XAMPP Control Panel 3.2.4
	...

Überarbeitungen: Anfang 2021 für Aktualisierte 7.4 Version (7.4.14)
Nötige Software:
-   7-ZIP 
-	ImageMagick
ausgelagert nach c:\xampp-cmsod\_tools\...

Ordner _tools komplett über Git-Repo organisiert (s.u.).

Aktualisierungen und Tests:
===========================
- PHP 7.4
- TYPO3 10.4
- Joomla 3.9 / Joomla 4 Beta
- WordPress 5.6

Aktualisierungen Ordner _tools mit Git:
=======================================
git clone https://gitlab.com/joebrandes/_tools.git




Überarbeitungen: ab Ende März 2020 (PHP 7.4 !!)
vor Seminarwochen (BUs) Joomla 4, TYPO3 10 ab Mitte 2020

Aktualisierungen Tools (Ordner: _tools)
=======================================
  Kein "Standard-Editor" mehr integriert!
  Grund: Notepad "nervt" mit Aktualisierungen/Kompatibilitäten
  Empfehlungen: 
	(User-)Installation von Microsoft Visual Studio Code
	Notepad++ selbst installieren oder ZIP-/NO-Install-Varianten
  
Aktualisierungen CMSOD-Overview Website
=======================================
  Inhalte für 4.0 aktualisiert und Fehler behoben
  Veröffentlichung geplant unter:
  http://overview.cms-online-designer.de 
  

 
  
TYPO3 10.3.0
============
  Anm.: 10.3.0 (letzte Vorversion vor LTS 10.4 ab vor. 21.04.2020)
  Installs OK -> ABER: es sollten VHosts genutzt werden! 
  (inkl. German language für BE, Extensions, Tests mit ImageMagick 7)
  Installarchive werden nicht mehr mit XAMPP-CMSOD ausgeliefert
  komplette Testing-Installation (TYPO3 10.3.0) unter http://www.typo3-testing.local (DB: typo3_testing)
  Backup/Restore Batch getestet backup-typo3-demo.bat (OK)
  
  Testseite ohne VHost: Probleme mit Dashboard-Darstellung, weil Pfad zu CSS für DB falsch verdrahtet wird.

Tests mit Vorversionen (Pre-v10):
  
TYPO3 9 sollte  gut funktionieren! 
Testinstallation: OK (mit: Sprachen, Extensions installieren, Page + Template)

TYPO3 8 benötigt windows-seitig unbedingt die Admin-Konfiguration für OpenSSL Variable!


 
Joomla 3.9.x 
============
  Install und Nutzung/Administration, Installation Template J51_scarlett_free
  Installationen über WebInstall Joomla: JCE 2.8.9, Akeeba 7.0.2
  Installarchive (werden nicht mehr im XAMPP-CMSOD integriert)
  komplette,deutsche Demo-Installation (Joomla 3.9.16) 
  
  Testinstallationen unter http://localhost/joomla/joomla-3.9 (DB: joomla_39)  
  erstellt, gesichert und wieder gelöscht! 

Joomla 3.10.0-DEV-20200319 (NightlyBuild) 
=============
  Install und Nutzung/Administration, Installation Template J51_scarlett_free
  Installationen über WebInstall Joomla: JCE 2.8.9, Akeeba 7.0.2
  Installarchive (werden nicht mehr im XAMPP-CMSOD integriert)
  komplette,deutsche Demo-Installation (Joomla 3.10.0-DEV-20200319) 
  
Joomla 4.0.0-beta1-dev NightlyBuild 20200319
========
  Install und Nutzung/Administration
  komplette Demo-Installation  
  Backup/Restore Batch getestet backup-joomla-demo.bat (OK)
  Installarchive (werden nicht mehr im XAMPP-CMSOD integriert)  
  
  
  
WordPress 5.3.2
========
  Install und Nutzung/Administration
  komplette Demo-Installation  
   
  

