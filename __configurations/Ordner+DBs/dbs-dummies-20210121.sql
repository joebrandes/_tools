-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Erstellungszeit: 21. Jan 2021 um 16:55
-- Server-Version: 10.4.17-MariaDB
-- PHP-Version: 7.4.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Datenbank: `joomla_demo`
--
DROP DATABASE IF EXISTS `joomla_demo`;
CREATE DATABASE IF NOT EXISTS `joomla_demo` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `joomla_demo`;
--
-- Datenbank: `joomla_seminar`
--
DROP DATABASE IF EXISTS `joomla_seminar`;
CREATE DATABASE IF NOT EXISTS `joomla_seminar` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `joomla_seminar`;
--
-- Datenbank: `muster_joomla`
--
DROP DATABASE IF EXISTS `muster_joomla`;
CREATE DATABASE IF NOT EXISTS `muster_joomla` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `muster_joomla`;
--
-- Datenbank: `muster_joomla_erg`
--
DROP DATABASE IF EXISTS `muster_joomla_erg`;
CREATE DATABASE IF NOT EXISTS `muster_joomla_erg` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `muster_joomla_erg`;
--
-- Datenbank: `muster_phpmysql`
--
DROP DATABASE IF EXISTS `muster_phpmysql`;
CREATE DATABASE IF NOT EXISTS `muster_phpmysql` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `muster_phpmysql`;
--
-- Datenbank: `muster_phpmysql_erg`
--
DROP DATABASE IF EXISTS `muster_phpmysql_erg`;
CREATE DATABASE IF NOT EXISTS `muster_phpmysql_erg` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `muster_phpmysql_erg`;
--
-- Datenbank: `muster_typo3`
--
DROP DATABASE IF EXISTS `muster_typo3`;
CREATE DATABASE IF NOT EXISTS `muster_typo3` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `muster_typo3`;
--
-- Datenbank: `muster_typo3_erg`
--
DROP DATABASE IF EXISTS `muster_typo3_erg`;
CREATE DATABASE IF NOT EXISTS `muster_typo3_erg` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `muster_typo3_erg`;
--
-- Datenbank: `muster_wordpress`
--
DROP DATABASE IF EXISTS `muster_wordpress`;
CREATE DATABASE IF NOT EXISTS `muster_wordpress` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `muster_wordpress`;
--
-- Datenbank: `muster_wordpress_erg`
--
DROP DATABASE IF EXISTS `muster_wordpress_erg`;
CREATE DATABASE IF NOT EXISTS `muster_wordpress_erg` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `muster_wordpress_erg`;
--
-- Datenbank: `phpmysql_demo`
--
DROP DATABASE IF EXISTS `phpmysql_demo`;
CREATE DATABASE IF NOT EXISTS `phpmysql_demo` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `phpmysql_demo`;
--
-- Datenbank: `phpmysql_seminar`
--
DROP DATABASE IF EXISTS `phpmysql_seminar`;
CREATE DATABASE IF NOT EXISTS `phpmysql_seminar` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `phpmysql_seminar`;
--
-- Datenbank: `pruef_joomla`
--
DROP DATABASE IF EXISTS `pruef_joomla`;
CREATE DATABASE IF NOT EXISTS `pruef_joomla` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `pruef_joomla`;
--
-- Datenbank: `pruef_joomla_erg`
--
DROP DATABASE IF EXISTS `pruef_joomla_erg`;
CREATE DATABASE IF NOT EXISTS `pruef_joomla_erg` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `pruef_joomla_erg`;
--
-- Datenbank: `pruef_phpmysql`
--
DROP DATABASE IF EXISTS `pruef_phpmysql`;
CREATE DATABASE IF NOT EXISTS `pruef_phpmysql` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `pruef_phpmysql`;
--
-- Datenbank: `pruef_phpmysql_erg`
--
DROP DATABASE IF EXISTS `pruef_phpmysql_erg`;
CREATE DATABASE IF NOT EXISTS `pruef_phpmysql_erg` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `pruef_phpmysql_erg`;
--
-- Datenbank: `pruef_typo3`
--
DROP DATABASE IF EXISTS `pruef_typo3`;
CREATE DATABASE IF NOT EXISTS `pruef_typo3` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `pruef_typo3`;
--
-- Datenbank: `pruef_typo3_erg`
--
DROP DATABASE IF EXISTS `pruef_typo3_erg`;
CREATE DATABASE IF NOT EXISTS `pruef_typo3_erg` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `pruef_typo3_erg`;
--
-- Datenbank: `pruef_wordpress`
--
DROP DATABASE IF EXISTS `pruef_wordpress`;
CREATE DATABASE IF NOT EXISTS `pruef_wordpress` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `pruef_wordpress`;
--
-- Datenbank: `pruef_wordpress_erg`
--
DROP DATABASE IF EXISTS `pruef_wordpress_erg`;
CREATE DATABASE IF NOT EXISTS `pruef_wordpress_erg` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `pruef_wordpress_erg`;
--
-- Datenbank: `typo3_demo`
--
DROP DATABASE IF EXISTS `typo3_demo`;
CREATE DATABASE IF NOT EXISTS `typo3_demo` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `typo3_demo`;
--
-- Datenbank: `typo3_seminar`
--
DROP DATABASE IF EXISTS `typo3_seminar`;
CREATE DATABASE IF NOT EXISTS `typo3_seminar` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `typo3_seminar`;
--
-- Datenbank: `wordpress_demo`
--
DROP DATABASE IF EXISTS `wordpress_demo`;
CREATE DATABASE IF NOT EXISTS `wordpress_demo` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `wordpress_demo`;
--
-- Datenbank: `wordpress_seminar`
--
DROP DATABASE IF EXISTS `wordpress_seminar`;
CREATE DATABASE IF NOT EXISTS `wordpress_seminar` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `wordpress_seminar`;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
