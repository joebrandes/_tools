# _tools

Ziel: PHP 8.2 Umgebung

XAMPP-CMSOD Ordner für Tools und Zusatztechniken rund um den CMSOD (CMS Online Designer)


![Status: 8.2.12 Apachefriends.org 2025](./_pics/xampp-8.2.12-status.png)

Zielordner: ``C:\xampp-cmsod\_tools``

	Wenn man direkt klont, dann bitte später den .git Ordner löschen!
	Siehe Datenmenge/Größe!

Datei: ``README_DE-CMSOD-PHP-8.2-2025.txt`` (bzw. Versions-/Loggingdatei) in ``C:\xampp-cmsod\``

Vorgehen: Zip (oder 7z) der Apachefriends.org-Community in ``C:\xampp-cmsod``
entpacken und mit Hilfe diese Repos in technische Form bringen.

Nicht vergessen: run ``setup_xampp.bat`` um alle Pfade zu setzen!

## Roadmaps

Kurzübersicht zu **PHP** Unterstützungen und LifeCycles

*   PHP: https://www.php.net/supported-versions.php
*   Joomla 5.x: https://manual.joomla.org/docs/next/get-started/technical-requirements/
*   TYPO3 v13: https://get.typo3.org/version/13#system-requirements
*   WordPress 6.7: https://make.wordpress.org/hosting/handbook/server-environment/


## Ordner 

Kurzerläuterungen zu Ordnern:

```
C:\xampp-cmsod\_tools
├──7-Zip                   Version 24.09
├──cmsod-overview          Website für CMSOD
├──ImageMagick-7.0.7-0     Grafiktool für TYPO3
├──_pics                   Beispielbilder und XAMPP 
├──_templates              Bootstrap und Div
├──__backup                Skripttechnik für Backups
├──__backupZIPs            Ordner für erstellte Backups 
└──__configurations        Konfigurationen/Anpassungen 
```

## Demo-Installationen

Ich habe für die drei CMS Demo-Installationen vorgenommen:

*   Joomla: admin / Password: cmsodcmsodcmsod
*   TYPO3: admin / Password: Cmsod2025+Cmsod2025
*   WordPress: admin /Password: cmsodcmsod

## To-Do:

*	Techniken mit PHP 8.2 und diversen CMS testen
*   Roadmap beobachten und PHP 8.4 Umsetzung finden (Apachefriends Ende?)
*	Newer ImageMagick than 7.0.7-0 ?

