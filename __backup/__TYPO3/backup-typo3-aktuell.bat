@echo off
SETLOCAL

rem ----Greetings -----------------------------------
echo **********************************************
echo * 1-click Sicherung CMSOD                    *
echo * bitte einfach den Anweisungen folgen...    *
echo **********************************************
echo *                                            *

rem ----Pr�fungsteilnehmer einlesen------------------
echo **********************************************
echo * Bitte Benutzernamen in Form                *
echo * Nachname-Vorname                           *
echo **********************************************
echo * eingeben:                                  *
set /p Benutzer=

rem ----EINSTELLUNGEN--------------------------------
rem Ordner immer ohne abschlieszenden \ Backslash
rem Hauptordner fuer Backups:
set "Backups=c:\xampp-cmsod\_tools\__backupZIPs"
rem Arbeitsverzeichnis fuer 7-Zip Portable:
set "SevenZipPath=C:\xampp-cmsod\_tools\7-Zip"
rem Hauptverzeichnis MySQL des XAMPP-CMSOD:
set "MySQLPath=C:\xampp-cmsod\mysql"
rem Hauptordner WWW bzw. htdocs bei XAMPP-CMSOD:
set "WWWhtdocsOrdnerPath=C:\xampp-cmsod\htdocs"
rem Zugangsdaten zur DB MySQL des XAMPP-CMSOD:
set "MySQLUser=root"
set "MySQLPwd=NULL"


rem Order fuer die zu erstellende Sicherung:
rem Hauptordner:
rem set "WWWhtdocsHauptordner=pruefungen"
set "WWWhtdocsHauptordner=typo3"
rem Detaillordner:
set "WWWhtdocsOrdner=www.typo3-aktuell.local"
rem DB fuer die zu erstellende Sicherung:
rem set "MySQLDB=pruef_phpmysql"
rem set "MySQLDB=pruef_joomla"
rem set "MySQLDB=pruef_typo3"
set "MySQLDB=typo3_aktuell"
rem set "MySQLDB=cdcol"


rem -------------------------------------------------
rem Sicherungsdatum erstellen:
set "BackupDate=%date:~-4%%date:~-7,2%%date:~-10,2%"
rem set "BackupTime=%time:~-11,2%%time:~-8,2%%time:~-5,2%"

rem set "BackupFile=%BackupDate%_%BackupTime%.sql"
rem SQL-Dateinamen zuweisen: Benuter_DB_Datum.sql
set "BackupFile=%Benutzer%_%MySQLDB%_%BackupDate%.sql"

rem %MySQLPath:~0,2%

rem in Arbeitsverzeichnis des MySQL wechseln:
cd %MySQLPath%\bin

echo Datenbank-Dump wird erstellt... [Datenbank: %MySQLDB%]
echo beim Passwort bitte einfach Return druecken (leeres Kennwort)

mysqldump -u%MySQLUser% --add-drop-database -p --databases %MySQLDB% > "%Backups%\%BackupFile%"

rem in WWW/htdocs Hauptordner:
rem cd %WWWhtdocsOrdnerPath%

rem mit 7-Zip Ordnerarchiv erstellen: Benutzer_Ordner_Datum.zip:
%SevenZipPath%\7z a %Backups%\%Benutzer%_%WWWhtdocsHauptordner%_%WWWhtdocsOrdner%_%BackupDate%.zip %WWWhtdocsOrdnerPath%\%WWWhtdocsHauptordner%\%WWWhtdocsOrdner%
rem mit 7-Zip SQL-Dump in Ordnerarchiv packen:
%SevenZipPath%\7z u %Backups%\%Benutzer%_%WWWhtdocsHauptordner%_%WWWhtdocsOrdner%_%BackupDate%.zip %Backups%\%BackupFile%
rem MySQL-Dump loeschen:
del %Backups%\%BackupFile%


echo **********************************************
echo * Bitte checken Sie das erstellte Archiv     *
echo * Bitte sichern Sie das erstellte Archiv     *                           
echo * Gesamtarchiv: %Benutzer%_%WWWhtdocsHauptordner%_%WWWhtdocsOrdner%_%BackupDate%.zip                             
echo **********************************************

pause Ende der Sicherung mit Tastendruck - Danke ...
