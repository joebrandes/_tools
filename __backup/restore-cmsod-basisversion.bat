@echo off
SETLOCAL

rem ----Greetings -----------------------------------
echo **********************************************
echo * 1-click Wiederherstellung CMSOD            *
echo * Wollen Sie wirklich Projekte erzeugen?     *
echo * bitte einfach den Anweisungen folgen...    *
echo **********************************************
echo *                                            *

rem ----Daten einlesen------------------------------
echo **********************************************
echo * Bitte Haupt-Projekt in Form                *
echo * pruefungen / musterpruefungen /            *
echo * phpmysql / joomla / typo3 / wordpress      *
echo **********************************************
echo * eingeben:                                  *
set /p WWWhtdocsHauptordner=

echo *********************************************************************
echo * Bitte Teil Projekt in Form                                        *
echo * phpmysql / joomla / typo3                                         *
echo * beispiele / joomla-test / typo3-testing / www.typo3-demo.local    *
echo *********************************************************************
echo * eingeben:                                                         *
set /p WWWhtdocsOrdner=


rem ----EINSTELLUNGEN--------------------------------
rem Ordner immer ohne abschlieszenden \ Backslash
rem Hauptordner fuer Backups:
set "Backups=c:\xampp-cmsod\_tools\__backupZIPs"
rem Arbeitsverzeichnis fuer 7-Zip Portable:
set "SevenZipPath=C:\xampp-cmsod\_tools\7-Zip"
rem Hauptverzeichnis MySQL des XAMPP-CMSOD:
set "MySQLPath=C:\xampp-cmsod\mysql"
rem Hauptordner WWW bzw. htdocs bei XAMPP-CMSOD:
set "WWWhtdocsOrdnerPath=C:\xampp-cmsod\htdocs"
rem Zugangsdaten zur DB MySQL des XAMPP-CMSOD:
set "MySQLUser=root"
set "MySQLPwd=NULL"


rem mit 7-Zip Ordnerarchiv auspacken: Benutzer_Hauptordner_Ordner_Datum.zip:
%SevenZipPath%\7z x %1 -o%WWWhtdocsOrdnerPath%\%WWWhtdocsHauptordner%


cd %WWWhtdocsOrdnerPath%\%WWWhtdocsHauptordner%
For /F %%i in ('dir /B *.sql') do set SqlDatei=%%~ni

rem echo %SqlDatei%.sql
%MySQLPath%\bin\mysql -u%MySQLUser% -p < %SqlDatei%.sql

del %SqlDatei%.sql

echo **********************************************
echo * Bitte checken Sie die erstellten Ordner    *
echo * Bitte checken Sie die erstellten DBs      *                           
echo **********************************************

pause Ende der Ruecksicherung mit Tastendruck - Danke ...
