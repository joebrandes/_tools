@echo off
SETLOCAL

rem Ein Skript to "rule them all"
rem =============================
rem Author: Joe Brandes
rem Date: 2020
rem created for CMSOD 4.0

rem ----Greetings -----------------------------------
echo ***************************************************************************************
echo * 1-click Sicherung XAMPP-CMSOD                                                       *
echo * bitte einfach den Anweisungen folgen...                                             *
echo ***************************************************************************************
echo * Auswahl der Sicherungen:                                                            *
echo * (1) www.typo3-testing.local in Ordner: /typo3/. mit DB typo3_testing                *
echo * (2) www.typo3-muster.local in Ordner: /musterpruefungen/. mit DB muster_typo3       *
echo * (...)                                                                               *
echo ***************************************************************************************
echo * eingeben:                                                                           *
set /p Auswahl=

rem ----EINSTELLUNGEN--------------------------------
rem Ordner immer ohne abschlieszenden \ Backslash
rem Hauptordner fuer Backups:
set "Backups=c:\xampp-cmsod\_tools\__backupZIPs"
rem Arbeitsverzeichnis fuer 7-Zip Portable:
set "SevenZipPath=C:\xampp-cmsod\_tools\7-Zip"
rem Hauptverzeichnis MySQL des XAMPP-CMSOD:
set "MySQLPath=C:\xampp-cmsod\mysql"
rem Hauptordner WWW bzw. htdocs bei XAMPP-CMSOD:
set "WWWhtdocsOrdnerPath=C:\xampp-cmsod\htdocs"
rem Zugangsdaten zur DB MySQL des XAMPP-CMSOD / Apachefriends.org:
set "MySQLUser=root"
set "MySQLPwd=NULL"

rem Auswahl treffen
rem ===============

rem www.typo3-testing.local (Pfad: ./htdocs/typo3/www.typo3-testing.local)
If "%Auswahl%" == "1" GOTO Backup1 
rem www.typo3-muster.local (Pfad: ./htdocs/musterpruefungen/typo3)
If "%Auswahl%" == "2" GOTO Backup2

rem sonst bitte abbrechen
rem pause>nul
rem ping -n 5 localhost > nul
exit

:Backup1
rem Order fuer die zu erstellende Sicherung: (fuer alternative Projekte kopieren/durchnummerieren
rem Hauptordner:
set "WWWhtdocsHauptordner=typo3"
rem Detaillordner:
set "WWWhtdocsOrdner=www.typo3-testing.local"
rem DB fuer die zu erstellende Sicherung:
set "MySQLDB=typo3_testing"
rem Abarbeitung fortsetzen
goto Weiter

:Backup2
rem Order fuer die zu erstellende Sicherung: (fuer alternative Projekte kopieren/durchnummerieren
rem Hauptordner:
set "WWWhtdocsHauptordner=musterpruefungen"
rem Detaillordner:
set "WWWhtdocsOrdner=typo3"
rem DB fuer die zu erstellende Sicherung:
set "MySQLDB=muster_typo3"
rem Abarbeitung fortsetzen
goto Weiter

:Backup100
pause>nul


:Weiter
rem ----Projektdaten- bzw. Teilnehmer einlesen------------------
echo ***************************************************************************************
echo * Bitte Projektdataten oder Teilnehmer in Form                                        *
echo * Nachname-Vorname (oder: TYPO3-testing-01)                                           *
echo ***************************************************************************************
echo * eingeben:                                                                           *
set /p Benutzer=
rem -------------------------------------------------
rem Sicherungsdatum erstellen:
set "BackupDate=%date:~-4%%date:~-7,2%%date:~-10,2%"
rem set "BackupTime=%time:~-11,2%%time:~-8,2%%time:~-5,2%"
set "BackupTime=%time:~-11,2%%time:~-8,2%"


rem set "BackupFile=%BackupDate%_%BackupTime%.sql"
rem SQL-Dateinamen zuweisen: Benuter_DB_Datum.sql
set "BackupFile=%Benutzer%_%MySQLDB%_%BackupDate%-%BackupTime%.sql"

rem in Arbeitsverzeichnis des MySQL wechseln:
cd %MySQLPath%\bin

echo Datenbank-Dump wird erstellt... [Datenbank: %MySQLDB%]
echo Beim Passwort bitte einfach Return druecken (leeres Kennwort)

mysqldump -u%MySQLUser% --add-drop-database -p --databases %MySQLDB% > "%Backups%\%BackupFile%"

rem mit 7-Zip Ordnerarchiv erstellen: Benutzer_Ordner_Datum.zip:
%SevenZipPath%\7z a %Backups%\%Benutzer%_%WWWhtdocsHauptordner%_%WWWhtdocsOrdner%_%BackupDate%-%BackupTime%.zip %WWWhtdocsOrdnerPath%\%WWWhtdocsHauptordner%\%WWWhtdocsOrdner%
rem mit 7-Zip SQL-Dump in Ordnerarchiv packen:
%SevenZipPath%\7z u %Backups%\%Benutzer%_%WWWhtdocsHauptordner%_%WWWhtdocsOrdner%_%BackupDate%-%BackupTime%.zip %Backups%\%BackupFile%
rem MySQL-Dump loeschen:
del %Backups%\%BackupFile%

echo ***************************************************************************************
echo * Bitte checken Sie das erstellte Archiv                                              *
echo * Bitte sichern Sie das erstellte Archiv                                              *
echo * Gesamtarchiv: %Benutzer%_%WWWhtdocsHauptordner%_%WWWhtdocsOrdner%_%BackupDate%-%BackupTime%.zip
echo ***************************************************************************************

pause Ende der Sicherung mit Tastendruck - Danke ...
